<?php
error_reporting( E_ALL );
ini_set('display_errors', 'on');

$cnf = parse_ini_file("/home/ghidorah/.webconfig.ini");
include "lastFM.php";

header("Content-type: application/json");

function fixArtistName($an) {
//    if (preg_match('/\(([^)]*)\)/', $an, $matches)) {
    if (preg_match('/(.[^(]+)/', $an, $matches)) {
        return trim($matches[1]);
    } else {
        return $an;
    }
}

$db = mysqli_connect($cnf["maria_host"], $cnf["maria_user"], $cnf["maria_pass"], $cnf["maria_db"])
    or die(mysql_error());

$query="SELECT * FROM trackdata";
$data = mysqli_query($db,$query);
$rows = array();
while($r = mysqli_fetch_assoc($data)) {
    $rows[$r["keyid"]] = $r["value"];
}

$query="SELECT image FROM imagecache WHERE artist=\"{$rows['current_artist']}\" AND album=\"{$rows['current_album']}\";";
$data = mysqli_query($db,$query);
    $r = mysqli_fetch_assoc($data);	
if ($r["image"] != 'null') {


    $rows["album_image_origin"]="database";
    $rows["album_image"]=$r["image"];

} else {

    $fan=fixArtistName($rows["current_artist"]);
    $lfm=getAlbum($fan,$rows["current_album"],2);
    $rows["debug"]=$fan;
    if ($lfm) {
        $rows["album_image_origin"]="lastFM";
        $rows["album_image"]=$lfm;
        $query="REPLACE INTO imagecache (artist, album, image) VALUES (\"{$rows['current_artist']}\",\"{$rows['current_album']}\",\"{$lfm}\")";
        mysqli_query($db,$query);
    } else {
        $rows["album_image_origin"]="not found";
        $rows["album_image"]="/noimg.gif";
    }
}

$query="SELECT image FROM imagecache WHERE artist=\"{$rows['current_artist']}\" AND album IS NULL;";
$data = mysqli_query($db,$query);
if ($data) {
    $r = mysqli_fetch_assoc($data);

    $rows["artist_image"]=$r["image"];
} else {
    $lfm=getArtistPhoto(fixArtistName($rows["current_artist"]),2);
    if ($lfm) {
        $rows["artist_image"]=$lfm;
        $query="REPLACE INTO imagecache (artist, album, image) VALUES (\"{$rows['current_artist']}\",NULL,\"{$lfm}\")";
        mysqli_query($db,$query);
    } else {
        $rows["artist_image"]="/noimg.gif";
    }
}

echo json_encode($rows);


