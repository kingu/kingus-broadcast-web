<!DOCTYPE html>
<html>
  <head>
    <title>Kingu's Broadcast</title><!--'-->
    <meta property="og:title" content="Kingu's Broadcast" />
    <meta property="og:description" content="Music!" />
    <meta property="og:image" content="./slides/1.jpg" />
    <link href="data:image/x-icon;base64,AAABAAEAEBAQAAAAAAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAgICCAP///wDaybcA9xHdAK6YgAD99/EA9JT3AM68qQDz6dsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFVVVVUAAAAImZmZmVAAABIiIiIiYQARYgIiIiApEQhiAiIiIClQEWIiIiIiaREIYiIiIiR5UAhiIiJmd0dACDIiJkR3R0AIYiZkdHdHQAhiaZR0REQACGZVVXd3ZQAAAAAARAAAAAAAAAAAAAAAAAAAAAAAAAD//wAA8A8AAOAHAADAAwAAAAAAAIABAAAAAAAAgAEAAIABAACAAQAAgAEAAIADAACAAwAA/z8AAP//AAD//wAA" rel="icon" type="image/x-icon" />

    <script src="https://code.jquery.com/jquery-3.2.0.min.js"   
            integrity="sha256-JAW99MJVpJBGcbzEuXk4Az05s/XyDdBomFqNlM3ic+I="   
            crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.0.11/css/fork-awesome.min.css" integrity="sha256-MGU/JUq/40CFrfxjXb5pZjpoZmxiP2KuICN5ElLFNd8=" crossorigin="anonymous">

      <script src="jquery.jplayer.min.js"></script>
    <script>
      var urlParams;
      var currentTrack;
      (window.onpopstate = function () {
      var match,
      pl     = /\+/g,  // Regex for replacing addition symbol with a space
      search = /([^&=]+)=?([^&]*)/g,
      decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
      query  = window.location.search.substring(1);

      urlParams = {};
      while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2]);
      })();
      function randomString(length, chars) {
      var mask = '';
      if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
      if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      if (chars.indexOf('#') > -1) mask += '0123456789';
      if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';  /* " */
	var result = '';
	for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
	return result;
	}
	var bandcampLink;

	function gotoBandcamp () {
	var win=window.open(bandcampLink, '_blank');
	}

	var wikiLink;

	
	function gotoWikipedia () {
	var win=window.open(wikiLink, '_blank');
	}

var youtubeLink;
function gotoYoutube () {
    var win=window.open(youtubeLink, '_blank');
}
	var lastFMLink;	
	function gotoLastFM () {
	var win=window.open(lastFMLink, '_blank');
	}



	function withIcecastData(f) {
	$.getJSON("https://stream.streamaudio.de:8000/status-json.xsl?mount=/kingus-broadcast", function(data) {
	f(data.icestats);
	});     
	}
	function init(i) {
	img="./slides/"+i+".jpg";
	$.ajax({
	url: img,
	type: "HEAD",
	error: function() { init(0); },
	success: function() {
	$.getJSON("getter/?x="+randomString(16, '#aA'), function(data) {
//	console.log(data);
	$("#trackTitle").html(data["current_title"]);
	$("#trackArtist").html(data["current_artist"]);
    bandcampLink="https://bandcamp.com/search?q="+data["current_artist"];
	wikiLink="https://en.wikipedia.org/w/index.php?search="+data["current_artist"];+"&title=Special:Search&go=Go";
    youtubeLink="https://www.youtube.com/results?search_query="+data["current_artist"];
    lastFMLink="https://www.last.fm/search?q="+data["current_artist"];
	$("#trackAlbum").html(data["current_album"]);
	$("#trackGenre").html(data["current_genre"]);
	$("#mood").val(data["current_mood"]);
	$("#cover").attr("src",(data["album_image"]));
	$("#artist").attr("src",(data["artist_image"]));
        $.get("/getter/getplaylist.php", function (data) { $("#tabs-1").html(data); });


	    
	withIcecastData(function(d) {
        if (d.source) { 
        $("#listeners").html(d.source.listeners);
        $("#player").css("background-color", "black");
        } else {
        $("#player").css("background-color", "red");
        }
        });
	$("#slideImg").attr("src", img );
	setTimeout(function () { init(i+1); }, 10000); 
	});
	}
	});
	}

	$(document).ready(function(){
	init(0);

    $('.spinlink').hover(
        function(){ $(this).addClass('fa-spin') },
        function(){ $(this).removeClass('fa-spin') }
    )

    
	var stream = { title: "Kingu", mp3: "https://stream.streamaudio.de:8000/kingus-broadcast" },     
	
	ready = false;
	
	$("#sysplayer").jPlayer({
	ready: function (event) {
	ready = true;
	$(this).jPlayer("setMedia", stream);
	},
	pause: function() {
	$(this).jPlayer("clearMedia");
	},
	error: function(event) {
	if(ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
	// Setup the media stream again and play it.
	$(this).jPlayer("setMedia", stream).jPlayer("play");
	}
	},
	swfPath: "/",
	supplied: "mp3",
	preload: "none",
	wmode: "window",
	cssSelectorAncestor: "#player", 
	//	    useStateClassSkin: true,
	autoBlur: false,
	keyEnabled: true
	});
	});

	function saveMood() {
	var mood=$("#mood").val()
	$("#currentMood").html(mood);
	$.get("/setter/setmood.php?mood="+mood);
	}

	$( function() {
		 $( "#tabs" ).tabs({ heightStyle: "auto"});
	} );
    </script>
    <link rel="stylesheet" href="jquery-ui/jquery-ui.min.css">
    
    <script src="jquery-ui/jquery-ui.min.js"></script>
    
    <style>
    @font-face {
	font-family: artifika;
	src: url(Artifika-Regular.ttf);
    }
    body { background-image: url(bg/bg<?PHP echo $_GET["bg"]?>.jpg);
	   background-repeat: repeat-y;
	   background-size: 100%;
	   color: white;
	   font-family: artifika; }
    #mainWrapper {
	margin: auto;
    }
    table { background-color: black; opacity:0.9;
            margin-left:auto;
            margin-right:auto;
            margin-bottom: 10px;
            border-style:solid;
            border-radius: 12px;
	  }

    #header {  }      
    #player { width: 600px;}
    #player button { background-color: black;
		     color:white;
		     border-style:none;
		     margin:3px;
		     padding: 5px; }
    #player button :link { color: white; text-decoration: none;}
    #player button :hover { color: #ff0080; }
    #trackTitle {  }
    #trackArtistWrapper {  }
    #trackAlbum { }
    #trackGenre { }
    .ui-tabs    {
	padding: 0 !important;
	border-style: none;
	opacity: 0.9;
	background-image: none;
    }
    .minilink { font-size: 22px; color: white;padding-left: 5px; }
    .minilink a:link { color: white; text-decoration: none;}
    .minilink a:visited { color: white; text-decoration: none;}
    .minilink a:hover { color: #ff0080; }

    #tabs > ul > li { background-color: #ff0080 !important; }
    

ul.playlists {
  height: 50px;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
}
ol.stats {
  height: 300px;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
}
ul#genresList {
  height: 900px;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  list-style-type: none;
}

    </style>
  </head>
  <body>
    <div id="sysplayer"></div>  
    
    <div id="mainWrapper">
      <?php
       $check = fsockopen("stream.streamaudio.de", "8000");
       if (!$check) {echo "Broadcast server is not responding, please try again later"; }
       ?>
      
      <table>
    <tr><td colspan="3" style="text-align:center; background-image:url(wn.gif);"> <span class="fa-stack fa-lg">
    <i class="fa fa-heart fa-stack-1x big "></i>
      <i class="fa fa-ban fa-stack-2x text-danger" style="color:#ff0080;"></i>
</span> [Kingu&apos;s <i class="fa fa-heartbeat"></i> Broadcast] <span class="fa-stack fa-lg">
	      <i class="fa fa-music fa-stack-1x big "></i>
	      <i class="fa fa-ban fa-stack-2x text-danger" style="color:#ff0080;"></i>
	    </span></td></tr>
	<tr>
	  <td rowspan="5"><img id="cover" style="width:150px; height: 150px;" src="noimg.gif" alt="Album Cover"/></td>
	  <td id="player" style="font-weight: bold;width: 300px;">        <div class="center" style="width: 600px; margin: auto; font-size: 10px;">
	  
              <button id="play" class="jp-play"><i class="fa fa-play spinlink"></i></button>
              <button id="stop" class="jp-stop"><i class="fa fa-stop spinlink"></i></button>
              <a id="M3U" href="https://stream.streamaudio.de:8000/kingus-broadcast.m3u" class="myButton">M3U</a>
              
	      <a id="XSPF"  href="https://stream.streamaudio.de:8000/kingus-broadcast.xspf" class="myButton">XSPF</a>
<!--	      <select id="mood" onchange="saveMood()">
		<?php 
		   $rm=file_get_contents("moods");
		   
		   $srm = explode("\n", $rm);
		   foreach ($srm as $m) {
		   echo "<option>".$m."</option>";
		   }
		   ?>
	      </select> -->
	      &nbsp;[<span id="listeners"></span>]
               <!--<span class="minilink"><a href="http://pawoo.net/@kingu" target="new"><i class="fa fa-mastodon spinlink"></i></a></span>-->
	  </td>
	  <td rowspan="5"><img id="artist" style="width:150px; height: 150px;"></td>
	</tr>
	<tr>
	  <td id="trackTitle" style="font-weight: bold;width: 300px;"></td>
	</tr>
	<tr>
	  <td id="trackArtistWrapper" style="width: 300px;">
	    <span id="trackArtist" ></span>
	    
	    <span id="trackArtistWk" onclick="gotoWikipedia()" title="Wikipedia"><i class="fa fa-wikipedia-w spinlink"></i></span>
	    <span id="trackArtistBc" onclick="gotoBandcamp()" title="Bandcamp"><i class="fa fa-bandcamp spinlink"></i></span>
            <span id="trackArtistYt" onclick="gotoYoutube()" title="Youtube"><i class="fa fa-youtube spinlink"></i></span>
            <span id="trackArtistLf" onclick="gotoLastFM()" title="LastFM"><i class="fa fa-lastfm spinlink"></i></span>
	    
	  </td>
	</tr>
	<tr>
	  <td id="trackAlbum" style="font-style: italic;width: 300px;"></td>
	</tr>
	<tr>
	  <td id="trackGenre" style="width: 300px;"></td>
	</tr>	      
      </table>
      </div>
    </div>
    <div id="tabs" style="height:100%">
      <ul>
	<li><a href="#tabs-1"><i class="fa fa-list"></i> Current Playlist</a></li>
	<li><a href="#tabs-2"><i class="fa fa-plus"></i> Add tracks</a></li>
	<li><a href="#tabs-7"><i class="fa fa-cog"></i> Auto DJ</a></li>	
	<!--<li><a href="#tabs-5"><i class="fa fa-heart"></i> Playlists</a></li>-->
	<li><a href="#tabs-6"><i class="fa fa-commenting"></i> Add comment</a></li>
	<li><a href="#tabs-3"><i class="fa fa-bar-chart"></i> Stats</a></li>
	<li><a href="#tabs-4"><i class="fa fa-question"></i> About this page...</a></li>
      </ul>
      <div id="tabs-1"></div>
      
      <div id="tabs-2">
	<iframe src="/jukebox/jukebox.html" style="width: 100%;height:500px;"></iframe>
      </div>
          <div id="tabs-7">
	    <?php
	     include("tab-autodjconf.html");
	     ?>
	  </div>
  <!--    <div id="tabs-5">
<?php
//    include("tab-playlist.html");
?>
      </div>-->
      <div id="tabs-6">
<div id="disqus_thread"></div>
<script>

/**
 * *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
 * *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
 * var disqus_config = function () {
 * this.page.url = "http://kingu.reactoweb.com";  // Replace PAGE_URL with your page's canonical URL variable
 * this.page.identifier = "kingubroadcast"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
 * };
 * */
		   (function() { // DON'T EDIT BELOW THIS LINE
			   var d = document, s = d.createElement('script');
			   s.src = 'https://kingubroadcast.disqus.com/embed.js';
			   s.setAttribute('data-timestamp', +new Date());
			   (d.head || d.body).appendChild(s);
		   })();
		   </script>
			   <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	</div>
      <div id="tabs-3">
<?php
    include("tab-stats.html");
?>
      </div>


      <div id="tabs-4"><?php
			 include("tab-about.php");
?>
      </div>
    </div>
<script id="dsq-count-scr" src="//kingubroadcast.disqus.com/count.js" async></script>
  </body>
