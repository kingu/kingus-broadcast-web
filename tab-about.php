<p>This broadcast is starting from a Raspbery Pi model 2 installed in my house in Montreal</p> 
<h1>Mastodon</h1>
    <p>You can follow me on mastodon at <a href="https://octodon.social/@kingu_platypus_gidora" target="new">@kingu_platypus_gidora@octodon.social</a></p>

    <h1>Source</h1>
<p>The code of this web interface are at <a href="https://framagit.org/kingu/kingus-broadcast-web"></a>https://framagit.org/kingu/kingus-broadcast-web</p>
<p>The code of the server-side stuff are at <a href="https://framagit.org/kingu/kingus-broadcast-server"></a>https://framagit.org/kingu/kingus-broadcast-server</p>

    
<h1>Thanks</h1>
<p>To Mr Petovan for the web hosting</p>
<p>To streamaudio.de for the broadcasting</p>
<p>To <a href="https://babymetal.party/@asaka">Asaka secret kawaii collages</a> for the background images</p>

    <h1>API</h1>
    <p>To get information on current track<br>
      <code>https://kingu.reactoweb.com/getter/ </code><br/>
      Will result in json data like the following:<br/></p>
      <pre><?PHP 
function prettyPrint( $json )
{
    $result = '';
    $level = 0;
    $in_quotes = false;
    $in_escape = false;
    $ends_line_level = NULL;
    $json_length = strlen( $json );

    for( $i = 0; $i < $json_length; $i++ ) {
        $char = $json[$i];
        $new_line_level = NULL;
        $post = "";
        if( $ends_line_level !== NULL ) {
            $new_line_level = $ends_line_level;
            $ends_line_level = NULL;
        }
        if ( $in_escape ) {
            $in_escape = false;
        } else if( $char === '"' ) { //"
             $in_quotes = !$in_quotes;
        } else if( $char === '<' ) {
            $char="&st;";
        } else if( ! $in_quotes ) {
            switch( $char ) {
                case '}': case ']':
                    $level--;
                    $ends_line_level = NULL;
                    $new_line_level = $level;
                    break;

                case '{': case '[':
                    $level++;
                case ',':
                    $ends_line_level = $level;
                    break;

                case ':':
                    $post = " ";
                    break;

                
            case " ": case "\t": case "\n": case "\r":
                $char = "";
                    $ends_line_level = $new_line_level;
                    $new_line_level = NULL;
                    break;
            }
        } else if ( $char === '\\' ) {
            $in_escape = true;
        }
        if( $new_line_level !== NULL ) {
            $result .= "\n".str_repeat( "\t", $new_line_level );
        }
        $result .= $char.$post;
    }

    return $result;
}
echo prettyPrint(file_get_contents("https://kingu.reactoweb.com/getter/")); ?></pre>
	       
      

	       </div>  
 
